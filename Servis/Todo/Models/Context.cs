﻿using Microsoft.EntityFrameworkCore;

namespace Todo.Models
{
    public class Context: DbContext
    {
        private const string
            MESSAGE_EXCEPTION_NOT_VARIABLE = "Not variable for connection",
            NAME_ENVIRONMENT_VARIABLE = "CONNECTION";
        public readonly static string
            Connection;


        static Context()
        {
            var buff = Environment.GetEnvironmentVariable(NAME_ENVIRONMENT_VARIABLE, EnvironmentVariableTarget.User);
            if (buff != null)
                Connection = buff;
            else
                throw new Exception(MESSAGE_EXCEPTION_NOT_VARIABLE);
        }

        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Order> Orders { get; set; } = null!;

        public Context() => Database.EnsureCreated();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
            optionsBuilder.UseNpgsql(Connection);
        }
    }
}
