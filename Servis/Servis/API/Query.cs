﻿using Microsoft.EntityFrameworkCore;
using Todo.Models;

namespace Servis.API
{
    public class Query
    {
        [UseProjection]
        [UseFiltering]
        [UseSorting]
        public IQueryable<User> Read([Service] Context context) 
            => context.Users;
    }
}
