﻿using Todo.Models;

namespace Servis.API
{
    public class Subscription
    {
        [Subscribe] 
        public Order OnOrderSubscript ([EventMessage] Order order) => order;
        [Subscribe] 
        public User OnUserSubscript([EventMessage] User user) => user;
    }
}
