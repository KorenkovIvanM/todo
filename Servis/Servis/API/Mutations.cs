﻿using HotChocolate.Subscriptions;
using Todo.Models;

namespace Servis.API
{
    public class Mutations
    {
        public async Task<User> AddUser(User user, [Service] ITopicEventSender sander, [Service] Context context)
        {
            context.Users.Add(user);
            context.SaveChanges();

            await sander.SendAsync(nameof(Subscription.OnUserSubscript), user);

            return user;
        }

        public async Task<Order> AddOrder(Order order, [Service] ITopicEventSender sander, [Service] Context context)
        {
            context.Orders.Add(order);
            context.SaveChanges();

            await sander.SendAsync(nameof(Subscription.OnOrderSubscript), order);

            return order;
        }
    }
}
