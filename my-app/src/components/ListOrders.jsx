import { useQuery, gql } from '@apollo/client'
import React from 'react'

const GET_LOCATIONS = gql`
query{
    read(where: {id: {eq: 1}}){
      id
      name
      orders{
        id
        name
        description
      }
    }
  }
`

function ListOrders() {
    const { loading, error, data } = useQuery(GET_LOCATIONS);

    if (loading) return <p>Loading...</p>
    if (error) return <p>Error : {error.message}</p>

    console.log(data)

    return data.read[0].orders.map(({ id, name, description }) => (
        <div key={id}>
            <h3>{name}</h3>
            <br />
            <b>About this location:</b>
            <p>{description}</p>
            <br />
        </div>
    ))
}

export { ListOrders }